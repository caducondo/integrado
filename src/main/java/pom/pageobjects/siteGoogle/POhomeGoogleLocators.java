package pom.pageobjects.siteGoogle;

import br.com.link.setup.ConfigFramework;
import org.openqa.selenium.By;

public class POhomeGoogleLocators extends ConfigFramework {

    public static final By campoPesquisa = By.name("q");
    public static final By btnPesquisar = By.name("btnK");
}
