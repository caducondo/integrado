package pom.pageobjects.MobileEmbaixadores;

import br.com.link.setup.ConfigFramework;
import org.openqa.selenium.By;

public class PONewCaseLocators extends ConfigFramework {

    public By btnEnviar = By.id("br.com.g4solutions.cielo:id/action_submit");
    public By imputPedidoCliente = By.id("br.com.g4solutions.cielo:id/et_description");
    public By imputNomeResponsavel = By.id("br.com.g4solutions.cielo:id/et_name");
    public By imputDDDTelefone = By.id("br.com.g4solutions.cielo:id/et_phone");
    public By imputNomeFantasia = By.id("br.com.g4solutions.cielo:id/et_company");
    public By imputEC = By.id("br.com.g4solutions.cielo:id/et_ec");
    public By imputCPFouCNPJ = By.id("br.com.g4solutions.cielo:id/et_document");
    public By imputEmail = By.id("br.com.g4solutions.cielo:id/et_email");
    public By btnSentimentoOtimo = By.id("br.com.g4solutions.cielo:id/vw_feeling_otimo");
    public By btnSentimentoBom = By.id("br.com.g4solutions.cielo:id/vw_feeling_bom");
    public By btnSentimentoRegular = By.id("br.com.g4solutions.cielo:id/vw_feeling_regular");
    public By btnSentimentoRuim = By.id("br.com.g4solutions.cielo:id/vw_feeling_ruim");
    public By btnAddFoto = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ImageView");
    public By btnAddFotoCamera = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v7.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[1]");
    public By btnAddFotoGaleria = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v7.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]");
    public By btnAddFotoCancelar = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v7.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[3]");
    public By txtTitloNewCaseSucesso = By.id("br.com.g4solutions.cielo:id/textView5");
    public By txtCorpoNewCaseSucesso = By.id("br.com.g4solutions.cielo:id/textView6");

}