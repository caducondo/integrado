package pom.pageobjects.MobileEmbaixadores;

import br.com.link.setup.ConfigFramework;
import br.com.link.view.Actions;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import junit.framework.Assert;
import static br.com.link.asserts.Verifications.verifyElementIsVisible;
import static br.com.link.view.Actions.getText;

public class POHomeActions extends ConfigFramework {

    POHomeLocators locatorsHome = new POHomeLocators();
    POLoginLocators locatorsLogin = new POLoginLocators();

    public void loginComSucesso(String usuario, String senha) {
        Actions.setText(getAndroidDriver(),locatorsLogin.imputLogin,usuario,10);
        getAndroidDriver().hideKeyboard();
        Actions.setText(getAndroidDriver(),locatorsLogin.imputSenha,senha,10);
        getAndroidDriver().pressKey(new KeyEvent().withKey(AndroidKey.ENTER));
        Actions.clickOnElement(getAndroidDriver(),locatorsLogin.btnLogin,10);
        verifyElementIsVisible(getAndroidDriver(),locatorsHome.perfilLogado,10,true);
    }

    public void paginaHomeCarregadaComSucesso() {
        verifyElementIsVisible(getAndroidDriver(),locatorsHome.perfilLogado,10,true);
        Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.linkNewCase,10),"NEW CASE");
        Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.descricaoNewCase,10),"Solicitação, Reclamação, Sugestão, Elogio");
        Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.linkComeToCielo,10),"COME TO CIELO");
        Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.descricaoComeToCielo,10),"Indique Credenciamentos");
        Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.linkTrackCase,10),"TRACK CASE");
        Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.descricaoTrackCase,10),"Veja o andamento atualizado");
        Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.linkRecognition,10),"RECOGNITION");
        Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.descricaoRecognition,10),"Acompanhe sua pontuação");
        Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.linkCasosAguardandoEnvio,10),"CASOS AGUARDANDO ENVIO");
        //Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.linkPesquisaDeSatisfacao,3),"PESQUISA DE SATISFAÇÃO");
        Assert.assertEquals(getText(getAndroidDriver(), locatorsHome.linkChat,10),"CHAT");
    }
}