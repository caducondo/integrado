package pom.pageobjects.MobileEmbaixadores;

import br.com.link.documents.Documents;
import br.com.link.setup.ConfigFramework;
import br.com.link.view.Actions;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import junit.framework.Assert;
import static br.com.link.view.Actions.getText;

public class PONewCaseActions extends ConfigFramework {

    PONewCaseLocators locatorsNewCase = new PONewCaseLocators();

    public void cadastroNewCaseSucesso() {
        String cpfGerado = Documents.getCpf(false);
        Actions.setText(getAndroidDriver(),locatorsNewCase.imputPedidoCliente,"Pedido cliente "+cpfGerado,3);
        Actions.setText(getAndroidDriver(),locatorsNewCase.imputNomeResponsavel,"Nome cliente "+cpfGerado,3);
        getAndroidDriver().pressKey(new KeyEvent().withKey(AndroidKey.TAB));
        Actions.setText(getAndroidDriver(),locatorsNewCase.imputDDDTelefone,"11999887766",10);
        getAndroidDriver().pressKey(new KeyEvent().withKey(AndroidKey.TAB));
        Actions.setText(getAndroidDriver(),locatorsNewCase.imputNomeFantasia,"Nome Fantasia Cliente "+cpfGerado,3);
        getAndroidDriver().pressKey(new KeyEvent().withKey(AndroidKey.TAB));
        Actions.setText(getAndroidDriver(),locatorsNewCase.imputEC,cpfGerado,10);
        Actions.setText(getAndroidDriver(),locatorsNewCase.imputCPFouCNPJ,cpfGerado,10);
        getAndroidDriver().pressKey(new KeyEvent().withKey(AndroidKey.TAB));
        Actions.setText(getAndroidDriver(),locatorsNewCase.imputEmail,cpfGerado+"@mailinator.com",3);
        getAndroidDriver().pressKey(new KeyEvent().withKey(AndroidKey.TAB));
        Actions.clickOnElement(getAndroidDriver(),locatorsNewCase.btnSentimentoBom,10);
        //Actions.clickOnElement(getAndroidDriver(),btnEnviar,10);
        //verifyElementIsVisible(getAndroidDriver(),txtTitloNewCaseSucesso,10,true);
    }

    public void telaCadastroNewCaseSucesso() {
        Assert.assertEquals(getText(getAndroidDriver(), locatorsNewCase.txtTitloNewCaseSucesso,10),"Thanks!");
        Assert.assertEquals(getText(getAndroidDriver(), locatorsNewCase.txtCorpoNewCaseSucesso,10),"Your request has been received and within 3 working hours we will contact the customer");
    }
}