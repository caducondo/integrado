package pom.pageobjects.MobileEmbaixadores;

import br.com.link.setup.ConfigFramework;
import org.openqa.selenium.By;

public class POLoginLocators extends ConfigFramework {

    public By imputLogin = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[1]/TextInputLayout/android.widget.FrameLayout/android.widget.EditText");
    public By msgLogin = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[1]/TextInputLayout/android.widget.LinearLayout/android.widget.TextView");
    public By imputSenha = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[2]/TextInputLayout/android.widget.FrameLayout/android.widget.EditText");
    public By msgSenha = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[2]/TextInputLayout/android.widget.LinearLayout/android.widget.TextView");
    public By btnLogin = By.id("br.com.g4solutions.cielo:id/btn_login");
    public By msgLoginESenhaInvalido = By.id("android:id/message");
}