package pom.pageobjects.siteCielo;

import br.com.link.setup.ConfigFramework;
import org.openqa.selenium.By;

public class Site_CieloLocators extends ConfigFramework {

    public By maquininha = By.xpath("//a[.='Maquininhas']");

    public By saibaMais = By.className("prev_button");

//---------------------------------------- PAG ACESSE SUA CONTA --------------------------------------------------------


    public By btnEntrar = By.xpath("//button[@class='btn-submit-acesso link-top']");

    public By btnCrieSuaConta = By.xpath("//span[.='Crie sua conta']");

    public By btnSejaNossoCliente = By.xpath("//button[@class='btn btn-quaternary-shadow ']");

    public By campoEstabelecimneto =  By.xpath("//md-input-container[contains(.,'Estabelecimento Comercial')]");

    public By campoUsuaro = By.xpath("//input[@name='clientesUsuarioEstabelecimento']");

    public By msgBoasVindas = By.className("consultou");

    public By logoCielo = By.className("icons-logo pull-left ");

}
