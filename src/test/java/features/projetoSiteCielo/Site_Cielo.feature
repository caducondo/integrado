#language: pt
Funcionalidade: Funcionalidade Site Web Cielo
  Descrição breve da funcionalidade
  Eu como <operador> devo ser capaz de fazer algo

  @time5
  Cenario: CT100 - Descer até que o elemento fique visivel
    Dado que eu acesse o site "https://www.cielo.com.br"
    Quando descer a pagina para que o elemento "Maquininhas" estaja visivel
    Entao devo validar se o a pagina desceu corretamente

  @time5
  Cenario: CT101 - Clicar no link Maquininhas
    Dado que eu acesse o site "https://www.cielo.com.br"
    Quando eu clicar em Maquininha
    Entao devo validar se a pagina foi carregada corretamente

  @time5
  Cenario: CT102 - Inserir valor no campo usuario
    Dado que eu acesse o site "https://minhaconta2.cielo.com.br/login/"
    Quando inserir um valor no campo usuario
    Entao devo validar se o campo foi preenchido corretamente
    
  @time5
  Cenario: CT103 - Obter texto de boas vindas da pagina Cielo
    Dado que eu acesse o site "https://minhaconta2.cielo.com.br/login/"
    Quando obter a mensagem de boas vindas
    Entao devo validar se obteu a mensagem corretamente

  @time5
  Cenario: CT104 - Apagar texto no campo usario da pagina Cielo
    Dado que eu acesse o site "https://minhaconta2.cielo.com.br/login/"
    Quando limpar o conteudo que estiver no campo usuario
    Entao deve ser validado se o campo foi limpo com sucesso
    
  @time5
  Cenario: CT105 - Verificar se o campo submit Entrar pode ser clicado
    Dado que eu acesse o site "https://minhaconta2.cielo.com.br/login/"
    Entao devo validar se o botao entrar pode ser clicado