#language: pt
  @NORUN
Funcionalidade: Funcionalidade Site Web Android
  Descrição breve da funcionalidade
  Eu como <operador> devo ser capaz de fazer algo

  @time5
  Cenario: CT106 - Descer até que o elemento fique visivel
    Dado que inicio o browser android na pagina inicial da cielo pelo link "https://blog.cielo.com.br"
    Quando descer a pagina para que o elemento "Maquininhas" estaja visivel - android
    Entao devo validar se o a pagina desceu corretamente - android

  @time5
  Cenario: CT107 - Clicar no link Maquininhas
    Dado que inicio o browser android na pagina inicial da cielo pelo link "https://minhaconta2.cielo.com.br/login/"
    Quando eu clicar em Maquininha - android
    Entao devo validar se a pagina foi carregada corretamente - android

  @time5
  Cenario: CT108 - Inserir valor no campo usuario
    Dado que inicio o browser android na pagina inicial da cielo pelo link "https://minhaconta2.cielo.com.br/login/"
    Quando inserir um valor no campo usuario - android
    Entao devo validar se o campo foi preenchido corretamente - android

  @time5
  Cenario: CT109 - Obter texto de boas vindas da pagina Cielo
    Dado que inicio o browser android na pagina inicial da cielo pelo link "https://minhaconta2.cielo.com.br/login/"
    Quando obter a mensagem de boas vindas - android
    Entao devo validar se obteu a mensagem corretamente - android

  @time5
  Cenario: CT110 - Apagar texto no campo usario da pagina Cielo
    Dado que inicio o browser android na pagina inicial da cielo pelo link "https://minhaconta2.cielo.com.br/login/"
    Quando limpar o conteudo que estiver no campo usuario android
    Entao deve ser validado se o campo foi limpo com sucesso - android

  @time5
  Cenario: CT111 - Verificar se o campo submit Entrar pode ser clicado
    Dado que inicio o browser android na pagina inicial da cielo pelo link "https://minhaconta2.cielo.com.br/login/"
    Entao devo validar se o botao entrar pode ser clicado - android