#language: pt
Funcionalidade: Nova forma de buscar
  Validação dos erros na busca pelo site
  Eu como usuario devo ser capaz de realizar uma busca com sucesso

  @time3
  Cenario: Pesquisa por televisão
    Dado que eu acesse o site "http://google.com.br"
    Quando eu faço uma busca pelo termo "televisão"
    Entao o sistema apresenta o resultado "Aproximadamentes"

  @time3
  Cenario: Pesquisa por cotação do dollar
    Dado que eu acesse o site "http://google.com.br"
    Quando eu faço uma busca pelo termo "cotação do dollar"
    Entao o sistema apresenta o resultado "Aproximadamente"