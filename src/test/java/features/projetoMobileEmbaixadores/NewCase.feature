#language: pt
@NORUN
Funcionalidade: Pagina de New Case do Embaixadores Cielo
  Validação das regras de criação de uma New Case no App Embaixadores Cielo
  Eu como usuario devo ser capaz de criar um New Case no App Embaixadores Cielo

  @time4
  Cenario: Criação de New Case com sucesso
    Dado Que eu inicio o app Embaixadores Cielo do package "br.com.g4solutions.cielo" e acitivity "br.com.g4solutions.cielo.activity.SplashActivity"
    E eu realizar o login com usuario "JOAO.RENER42" e senha "345teste"
    E clico em New Case
    Quando eu preencher o formulario corretamente
    Entao deverá apresentar a tela de cadastro de New Case realizado com sucesso