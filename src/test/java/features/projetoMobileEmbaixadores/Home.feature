#language: pt
@NORUN
Funcionalidade: Pagina Home do Embaixadores Cielo
  Validação da validação da Home do App Embaixadores Cielo
  Eu como usuario devo ser capaz de visualizar a Home do App corretamente

  @time4 @funcional
  Cenario: Pagina home com todos seus elementos visiveis
    Dado Que eu inicio o app Embaixadores Cielo do package "br.com.g4solutions.cielo" e acitivity "br.com.g4solutions.cielo.activity.SplashActivity"
    Quando eu realizar o login com usuario "JOAO.RENER42" e senha "345teste"
    Entao devera apresentar todos os elementos na pagina