#language: pt
@NORUN
Funcionalidade: Pagina de login do Embaixadores Cielo
  Validação das regras de login do App Embaixadores Cielo
  Eu como usuario devo ser capaz de logar com usuario e senha validos

  @time4
  Cenario: login sem usr e senha
    Dado Que eu inicio o app Embaixadores Cielo do package "br.com.g4solutions.cielo" e acitivity "br.com.g4solutions.cielo.activity.SplashActivity"
    Quando eu clico no botão login
    Entao devera apresentar o aviso de usuario "Required field 'Login de rede'"

  @time4
  Cenario: login sem usr
    Dado Que eu inicio o app Embaixadores Cielo do package "br.com.g4solutions.cielo" e acitivity "br.com.g4solutions.cielo.activity.SplashActivity"
    Quando eu realizar o login com a senha "345tes"
    E eu clico no botão login
    Entao devera apresentar o aviso de usuario "Required field 'Login de rede'"

  @time4
  Cenario: login sem senha
    Dado Que eu inicio o app Embaixadores Cielo do package "br.com.g4solutions.cielo" e acitivity "br.com.g4solutions.cielo.activity.SplashActivity"
    Quando eu realizar o login com usuario "joao.rener42"
    E eu clico no botão login
    Entao devera apresentar o aviso de senha "Required field 'Senha'"

  @time4
  Cenario: senha invalida
    Dado Que eu inicio o app Embaixadores Cielo do package "br.com.g4solutions.cielo" e acitivity "br.com.g4solutions.cielo.activity.SplashActivity"
    Quando eu realizar o login com usuario "JOAO.RENER42"
    E eu realizar o login com a senha "asdfasdf"
    E eu clico no botão login
    Entao devera apresentar o alerta "Senha inválida, verifique e tente novamente."

  @time4
  Cenario: usuario invalida
    Dado Que eu inicio o app Embaixadores Cielo do package "br.com.g4solutions.cielo" e acitivity "br.com.g4solutions.cielo.activity.SplashActivity"
    Quando eu realizar o login com usuario "jdhfpaoihsefçk"
    E eu realizar o login com a senha "345teste"
    E eu clico no botão login
    Entao devera apresentar o alerta "Você não tem permissão para executar essa operação."

  @time4
  Cenario: login com sucesso
    Dado Que eu inicio o app Embaixadores Cielo do package "br.com.g4solutions.cielo" e acitivity "br.com.g4solutions.cielo.activity.SplashActivity"
    Quando eu realizar o login com usuario "JOAO.RENER42"
    E eu realizar o login com a senha "345teste"
    E eu clico no botão login
    Entao devera apresentar a tela "COLABORADOR"