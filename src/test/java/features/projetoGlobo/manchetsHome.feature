#language: pt
Funcionalidade: Nova home do site globo
  Validação dos erros na home do site da globo
  Eu como leitor devo ser capaz de ler o site da globo sem erros

  @time2
  Cenario: Validação do topo da home Globo
    Dado que eu acesse o site "http://globo.com.br"
    Quando eu navegar pela home
    Entao os links estarao corretos