package runner.projetoSiteCielo;

import br.com.link.setup.AppiumService;
import br.com.link.setup.ConfigFramework;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import static br.com.link.setup.ExtentLogger.setupExtentLogger;
import static br.com.link.setup.ExtentReports.setExtentReportsPath;
import static br.com.link.setup.ExtentReports.setupExtentReports;
import static br.com.link.setup.KlovReports.setupKlovReport;

@RunWith(Cucumber.class)
@CucumberOptions(
       features = "src/test/java/features/siteCielo",
       glue = {"stepdefinition","hooks"},
       tags = {},
       plugin = {
               "pretty",
               "html:target/cucumber-reports/cucumber-pretty",
               "json:target/cucumber-reports/CucumberTestReport.json",
               "rerun:target/cucumber-reports/rerun.txt",
               "junit:target/cukes/junit.xml",
               "com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:"
       })
public class SiteCieloCompleto extends ConfigFramework {
	  
	@BeforeClass
	public static void setUp() {
		setExtentReportsPath();
		setupExtentLogger();
		setupKlovReport();
	}

	@AfterClass
	public static void afterClass() {
		setupExtentReports();
		AppiumService.stopAppiumServer();
	}

}
