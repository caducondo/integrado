package stepdefinition.projetoMobileEmbaixadores;

import br.com.link.asserts.Verifications;
import br.com.link.setup.AppAndroid;
import br.com.link.setup.ConfigFramework;
import br.com.link.view.Actions;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import pom.pageobjects.MobileEmbaixadores.POHomeLocators;
import pom.pageobjects.MobileEmbaixadores.POLoginLocators;
import static br.com.link.asserts.Verifications.verifyTextsExistingElement;
import static br.com.link.view.AppActions.allowAppPermissions;

public class SDLogin extends ConfigFramework {

    POLoginLocators POLoginLocators = new POLoginLocators();
    POHomeLocators home = new POHomeLocators();

    @Dado("^Que eu inicio o app Embaixadores Cielo do package \"([^\"]*)\" e acitivity \"([^\"]*)\"$")
    public void que_eu_inicio_o_app_Embaixadores_Cielo_do_package_e_acitivity(String pack, String activity){
        AppAndroid app = new AppAndroid(getAndroidDriver());
        app.setUpDriver(app,pack, activity);
        setAndroidDriver(app.getDriver());
        allowAppPermissions(true);
    }

    @Quando("^eu clico no botão login$")
    public void eu_clico_no_botao_login() {
        getAndroidDriver().pressKey(new KeyEvent().withKey(AndroidKey.ENTER));
        Actions.clickOnElement(getAndroidDriver(),POLoginLocators.btnLogin,10);
    }

    @Quando("^eu realizar o login com a senha \"([^\"]*)\"$")
    public void eu_realizar_o_login_com_a_senha(String senhaDigitada) {
        Actions.setText(getAndroidDriver(),POLoginLocators.imputSenha,senhaDigitada,10);
    }

    @Quando("^eu realizar o login com usuario \"([^\"]*)\"$")
    public void eu_realizar_o_login_com_usuario(String usuarioDigitado) {
        Actions.setText(getAndroidDriver(),POLoginLocators.imputLogin,usuarioDigitado,10);
        getAndroidDriver().hideKeyboard();
    }

    @Entao("^devera apresentar o aviso de senha \"([^\"]*)\"$")
    public void devera_apresentar_o_aviso_de_senha(String mensagemSenha) {
        verifyTextsExistingElement(getAndroidDriver(),POLoginLocators.msgSenha,mensagemSenha,10,true);
    }

    @Entao("^devera apresentar o aviso de usuario \"([^\"]*)\"$")
    public void devera_apresentar_o_aviso_de_usuario(String mensagemUsuario) {
        verifyTextsExistingElement(getAndroidDriver(),POLoginLocators.msgLogin,mensagemUsuario,10,true);
    }

    @Entao("^devera apresentar o alerta \"([^\"]*)\"$")
    public void devera_apresentar_o_alerta(String mensagemDeErro) {
        Verifications.wait(5);
        verifyTextsExistingElement(getAndroidDriver(),POLoginLocators.msgLoginESenhaInvalido,mensagemDeErro,3,true);
    }

    @Entao("^devera apresentar a mensagem \"([^\"]*)\"$")
    public void devera_apresentar_a_mensagem(String mensagemHome) {
        verifyTextsExistingElement(getAndroidDriver(),home.perfilLogado,mensagemHome,10,true);
    }
}
