package stepdefinition.projetoMobileEmbaixadores;

import br.com.link.asserts.Verifications;
import br.com.link.setup.ConfigFramework;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pom.pageobjects.MobileEmbaixadores.POHomeActions;
import pom.pageobjects.MobileEmbaixadores.POHomeLocators;
import static br.com.link.asserts.Verifications.verifyTextsExistingElement;

public class SDHome extends ConfigFramework {

    POHomeLocators homepage = new POHomeLocators();
    POHomeActions acoesHomePage = new POHomeActions();

    @Entao("^devera apresentar a tela \"([^\"]*)\"$")
    public void devera_apresentar_a_tela(String mensagemHome)  {
        verifyTextsExistingElement(getAndroidDriver(),homepage.perfilLogado,mensagemHome,10,true);
    }

    @Quando("^eu realizar o login com usuario \"([^\"]*)\" e senha \"([^\"]*)\"$")
    public void eu_realizar_o_login_com_usuario_e_senha(String usuario, String senha) {
        acoesHomePage.loginComSucesso(usuario, senha);
    }

    @Entao("^devera apresentar todos os elementos na pagina$")
    public void devera_apresentar_todos_os_elementos_na_pagina() {
        acoesHomePage.paginaHomeCarregadaComSucesso();
        Verifications.wait(5);
    }
}
