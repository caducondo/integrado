package stepdefinition.projetoMobileEmbaixadores;

import br.com.link.setup.ConfigFramework;
import br.com.link.view.Actions;
import cucumber.api.java.es.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pom.pageobjects.MobileEmbaixadores.POHomeLocators;
import pom.pageobjects.MobileEmbaixadores.PONewCaseActions;
import pom.pageobjects.MobileEmbaixadores.PONewCaseLocators;
import static br.com.link.asserts.Verifications.verifyElementIsVisible;

public class SDNewCase extends ConfigFramework {

    POHomeLocators homepage = new POHomeLocators();
    PONewCaseLocators POnewCaseLocators = new PONewCaseLocators();
    PONewCaseActions POnewCaseActions = new PONewCaseActions();

    @Dado("^clico em New Case$")
    public void clico_em_New_Case() {
        Actions.clickOnElement(getAndroidDriver(),homepage.linkNewCase,3);
        verifyElementIsVisible(getAndroidDriver(),POnewCaseLocators.btnEnviar,3,true);
    }

    @Quando("^eu preencher o formulario corretamente$")
    public void eu_preencher_o_formulario_corretamente() {
        POnewCaseActions.cadastroNewCaseSucesso();
    }

    @Entao("^deverá apresentar a tela de cadastro de New Case realizado com sucesso$")
    public void devera_apresentar_a_tela_de_cadastro_de_New_Case_realizado_com_sucesso() {
        //newCase.telaCadastroNewCaseSucesso();
    }
}