package stepdefinition.projetoSiteCielo;

import br.com.link.setup.ConfigFramework;
import br.com.link.view.Actions;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.junit.Assert;
import org.openqa.selenium.By;
import pom.pageobjects.siteCielo.Site_CieloLocators;
import static br.com.link.setup.ExtentReports.appendToReport;
import static br.com.link.view.Actions.scrollToElement;

public class BaseSteps extends ConfigFramework {

    Site_CieloLocators pagInicial = new Site_CieloLocators();

//------------------------------------------------- CT 100 -------------------------------------------------------------

    @Quando("^descer a pagina para que o elemento \"([^\"]*)\" estaja visivel$")
    public void descerAPaginaParaQueOElementoEstajaVisivel(String arg0) throws Throwable {
        //Write code here that turns the phrase above into concrete actions
        scrollToElement(getBrowser(), pagInicial.maquininha,true,10);
    }

    @Entao("^devo validar se o a pagina desceu corretamente$")
    public void devoValidarSeOAPaginaDesceuCorretamente() {
        appendToReport(getBrowser(),true);
    }

//------------------------------------------------- CT 101 -------------------------------------------------------------

    @Quando("^eu clicar em Maquininha$")
    public void euClicarEmMaquininha() {
        scrollToElement(getBrowser(), pagInicial.maquininha,true,10);
        Actions.clickOnElement(getBrowser(), pagInicial.maquininha,10);
    }

    @Entao("^devo validar se a pagina foi carregada corretamente$")
    public void devoValidarSeAPaginaFoiCarregadaCorretamente() {
        Actions.getExistingElement(getBrowser(),By.className("bg-gradient-maquininhas"),20);
    }

//------------------------------------------------- CT 102 -------------------------------------------------------------

    @Quando("^inserir um valor no campo usuario$")
    public void inserirUmValorNoCampoUsuario() {
        Actions.setText(getBrowser(),pagInicial.campoUsuaro,"Cielo",10);
    }

    @Entao("^devo validar se o campo foi preenchido corretamente$")
    public void devoValidarSeOCampoFoiPreenchidoCorretamente() {
        appendToReport(getBrowser(),true);
    }

//------------------------------------------------- CT 103 -------------------------------------------------------------

    @Quando("^obter a mensagem de boas vindas$")
    public void obterAMensagemDeBoasVindas() {
        String msg = Actions.getText(getBrowser(), pagInicial.msgBoasVindas,10);
    }

    @Entao("^devo validar se obteu a mensagem corretamente$")
    public void devoValidarSeObteuAMensagemCorretamente() {
        String msg = Actions.getText(getBrowser(), pagInicial.msgBoasVindas,10);
        Assert.assertEquals("consultou.\n" +
                "Sua venda no extrato\n" +
                "em apenas 3 segundos", msg);
    }

//------------------------------------------------- CT 104 -------------------------------------------------------------

    @Quando("^limpar o conteudo que estiver no campo usuario$")
    public void limparOConteudoQueEstiverNoCampoUsuario() {
        Actions.setText(getBrowser(), pagInicial.campoUsuaro,"TXT QUE SERA" +
                " APAGADO",10);
        appendToReport(getBrowser(),true);
    }

    @Entao("^deve ser validado se o campo foi limpo com sucesso$")
    public void deveSerValidadoSeOCampoFoiLimpoComSucesso() {
        Actions.clearField(getBrowser(), pagInicial.campoUsuaro,10);
        appendToReport(getBrowser(),true);
    }

//------------------------------------------------- CT 105 -------------------------------------------------------------

    @Entao("^devo validar se o botao entrar pode ser clicado$")
    public void devoValidarSeOBotaoEntrarPodeSerClicado() {
        Actions.getClickableElement(getBrowser(), pagInicial.btnEntrar,10);
    }

//------------------------------------------------- Android -------------------------------------------------------------


//------------------------------------------------- CT 106 -------------------------------------------------------------

    @Quando("^descer a pagina para que o elemento \"([^\"]*)\" estaja visivel - android$")
    public void descerAPaginaParaQueOElementoEstajaVisivelAndroid(String arg0) throws Throwable {
        scrollToElement(getAndroidDriver(),pagInicial.saibaMais,true,10);
    }

    @Entao("^devo validar se o a pagina desceu corretamente - android$")
    public void devoValidarSeOAPaginaDesceuCorretamenteAndroid() {
        appendToReport(getAndroidDriver(),true);
    }

//------------------------------------------------- CT 107 -------------------------------------------------------------

    @Quando("^eu clicar em Maquininha - android$")
    public void euClicarEmMaquininhaAndroid() {
        Actions.clickOnElement(getAndroidDriver(),pagInicial.btnCrieSuaConta,10);
    }

    @Entao("^devo validar se a pagina foi carregada corretamente - android$")
    public void devoValidarSeAPaginaFoiCarregadaCorretamenteAndroid() {
        appendToReport(getAndroidDriver(),true);
    }

//------------------------------------------------- CT 108 -------------------------------------------------------------

    @Quando("^inserir um valor no campo usuario - android$")
    public void inserirUmValorNoCampoUsuarioAndroid() {
        Actions.setText(getAndroidDriver(),pagInicial.campoUsuaro,"Cielo",10);
    }

    @Entao("^devo validar se o campo foi preenchido corretamente - android$")
    public void devoValidarSeOCampoFoiPreenchidoCorretamenteAndroid() {
        appendToReport(getAndroidDriver(),true);
    }

//------------------------------------------------- CT 109 -------------------------------------------------------------

    @Quando("^obter a mensagem de boas vindas - android$")
    public void obterAMensagemDeBoasVindasAndroid() {
        String msg = Actions.getText(getAndroidDriver(), pagInicial.msgBoasVindas,10);
    }

    @Entao("^devo validar se obteu a mensagem corretamente - android$")
    public void devoValidarSeObteuAMensagemCorretamenteAndroid() {
        String msg = Actions.getText(getAndroidDriver(), pagInicial.msgBoasVindas,10);
        Assert.assertEquals("consultou.\n" +
                "Sua venda no extrato\n" +
                "em apenas 3 segundos", msg);
    }

//------------------------------------------------- CT 110 -------------------------------------------------------------

    @Quando("^limpar o conteudo que estiver no campo usuario android$")
    public void limparOConteudoQueEstiverNoCampoUsuarioAndroid() {
        Actions.setText(getAndroidDriver(), pagInicial.campoUsuaro,"TXT QUE SERA" +
                " APAGADO",10);
        appendToReport(getAndroidDriver(),true);
    }

    @Entao("^deve ser validado se o campo foi limpo com sucesso - android$")
    public void deveSerValidadoSeOCampoFoiLimpoComSucessoAndroid() {
        Actions.clearField(getAndroidDriver(), pagInicial.campoUsuaro,10);
        appendToReport(getAndroidDriver(),true);
    }

//------------------------------------------------- CT 111 -------------------------------------------------------------

    @Entao("^devo validar se o botao entrar pode ser clicado - android$")
    public void devoValidarSeOBotaoEntrarPodeSerClicadoAndroid() {
        Actions.getClickableElement(getAndroidDriver(), pagInicial.btnEntrar,10);
    }

}
