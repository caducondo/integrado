package stepdefinition.projetoGoogle;

import br.com.link.asserts.Verifications;
import br.com.link.setup.ConfigFramework;
import br.com.link.view.Actions;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pom.pageobjects.siteGoogle.POhomeGoogleLocators;
import pom.pageobjects.siteGoogle.POresultadoGoogleLocators;


public class BaseSteps extends ConfigFramework {

    @Quando("^eu faço uma busca pelo termo \"([^\"]*)\"$")
    public void eu_faço_uma_busca_pelo_termo(String termoPesquisa) {
        Actions.setText(getBrowser(), POhomeGoogleLocators.campoPesquisa,termoPesquisa,5);
        Actions.clickOnElement(getBrowser(),POhomeGoogleLocators.btnPesquisar,5);
    }

    @Entao("^o sistema apresenta o resultado \"([^\"]*)\"$")
    public void o_sistema_apresenta_o_resultado(String textoApresentado) {
        Verifications.verifyTextsExistingElement(getBrowser(), POresultadoGoogleLocators.txtQuantidadeResultados,textoApresentado,5, true);
    }
}
