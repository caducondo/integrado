package stepdefinition.projetoBackEnd;

import br.com.link.setup.ConfigFramework;
import br.com.link.setup.ExtentReports;
import cucumber.api.java.pt.Entao;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import pom.pageobjects.BackEnd.Livro;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class BaseSteps extends ConfigFramework {

    private RequestSpecification httpRequest = given();
    private Livro livro;
    private Livro livroEsperado;
    Response response;

    @cucumber.api.java.pt.Quando("^eu enviar uma requisicao Get para a API \"([^\"]*)\" com codigo (\\d+)$")
    public void euEnviarUmaRequisicaoGetParaAAPIComCodigo(String url, int param){
        String completUrl;
        completUrl = url + param;
        ExtentReports.appendToReport("Request: " + completUrl);
        response = httpRequest.get(completUrl);
        livro = response.as(Livro.class, ObjectMapperType.GSON);
        livroEsperado = new Livro(1l,false,"delectus aut autem",1L);
    }

    @Entao("^sera retornado com status HTTP (\\d+) e os dados com o valor de descricao de um livro$")
    public void seraRetornadoComStatusHTTPEOsDadosComOValorDeDescricaoDeUmLivro(int responseHttp){
        ExtentReports.appendToReport("Status Http: " + Integer.toString(response.getStatusCode()));
        ExtentReports.appendToReport(response.asString());
        assertThat(livro, samePropertyValuesAs(livroEsperado));
        assertEquals(responseHttp,response.getStatusCode());
    }
}
