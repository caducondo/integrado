package stepdefinition.projetoGlobo;

import br.com.link.asserts.Verifications;
import br.com.link.setup.ConfigFramework;
import cucumber.api.java.pt.Quando;
import cucumber.api.java.pt.Entao;
import pom.pageobjects.siteGlobo.POhomeGloboLocators;


public class BaseSteps extends ConfigFramework {

    @Quando("^eu navegar pela home$")
    public void eu_navegar_pela_home() {

    }

    @Entao("^os links estarao corretos$")
    public void os_links_estarao_corretos() {
        Verifications.verifyTextsExistingElement(getBrowser(), POhomeGloboLocators.topoPagina,"ASSINE JÁ",5, true);
    }
}
