package hooks;

import br.com.link.setup.AppAndroid;
import br.com.link.setup.AppWeb;
import br.com.link.setup.Drivers;
import br.com.link.setup.StartDriver;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;

import static br.com.link.setup.Drivers.closeDriver;

public class Hook extends StartDriver {

    @Dado("^que eu acesse o site \"([^\"]*)\"$")
    public void que_eu_acesse_o_site(String URL) {
        startBrowser();
        Drivers.loadApplication(getBrowser(), URL, true);
    }

    @Dado("^que inicio o browser android na pagina inicial da cielo pelo link \"([^\"]*)\"$")
    public void queInicioOBrowserAndroidNaPaginaInicialDaCieloPeloLink(String URL) {
        AppAndroid app = new AppAndroid(getAndroidDriver());
        app.setUpDriver(app,null);
        setAndroidDriver(app.getDriver());
        Drivers.loadApplication(getAndroidDriver(),URL,false);
    }

    @Before
    public void init() {
        super.setCucumberLanguage();

        /*Exemplo de start de driver para desktop
        startDesktop();                                                 //Utilizando parâmetros padrões definidos no Setup.properties
        startDesktop("C:/Windows/System32/notepad.exe");                //Utiliza o caminho passado para executar o programa especifico
        */

        /*Exemplo de start de driver para navegador
        startBrowser();                                                  //Utilizando parâmetros padrões definidos no Setup.properties
        startBrowser(BrowserOption.IE);                                 //Utiliza o parâmetro para definir o browser que será executado
        */
		
		/*Exemplo de start de driver Android
		startMobile();                                                  //Utilizando parâmetros padrões definidos no Setup.properties
		startMobile("src/test/resources/android/ApiDemos-Debug.apk");   //Utiliza o caminho para instalar e executar o APK especifico.
		startMobile("br.com.cielo", "MainActivity.activity");           //Utiliza o package e activity de um aplicativo já instalado para execução.
        */
    }

    @After
    public void cleanUp(Scenario scenario) {
        super.logReport(getBrowser(),scenario); 
		super.logReport(getDesktop(),scenario);
		super.logReport(getAndroidDriver(), scenario);
		
        closeDriver(getBrowser()); 
		closeDriver(getDesktop());
		closeDriver(getAndroidDriver());
    }

}
